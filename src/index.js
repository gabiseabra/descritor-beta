import React from "react"
import { compose, createStore, applyMiddleware, combineReducers } from "redux"
import { render } from "react-dom"
import { Provider } from "react-redux"
import { persistStore, autoRehydrate } from "redux-persist"
import { Router, Route, IndexRoute, hashHistory } from "react-router"
import { syncHistoryWithStore, routerMiddleware } from "react-router-redux"
import reducer from "./reducers"
import App from "./containers/App"
import Home from "./containers/HomeContainer"
import Stream from "./containers/StreamContainer"
import Book from "./containers/BookContainer"

let middleware = [ routerMiddleware(hashHistory) ];

if(process.env.NODE_ENV === "development") {
	middleware.push(require("redux-logger")())
}

const store = compose(
	applyMiddleware(...middleware),
	autoRehydrate()
)(createStore)(reducer)

persistStore(store, {
	blacklist: [ "routing" ]
})

const appHistory = syncHistoryWithStore(hashHistory, store)

render((
	<Provider store={store}>
		<Router history={appHistory}>
			<Route path="/" component={App}>
				<IndexRoute component={Home} />
				<Route path="/stream" component={Stream} />
				<Route path="/book" component={Book} />
				<Route path="/book/:id" component={Book} />
			</Route>
		</Router>
	</Provider>
), document.getElementById("app"))
