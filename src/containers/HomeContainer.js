import React, { Component, PropTypes } from "react"
import { connect } from "react-redux"
import { push } from "react-router-redux"
import { set, open, close, modify } from "../actions"
import { getDirectory } from "../reducers"
import { read } from "../api/Files";
import Home from "../components/Home"

class HomeContainer extends Component {
	static propTypes = {
		directory: PropTypes.string,
		recent: PropTypes.arrayOf(PropTypes.shape({
			file: PropTypes.string.isRequired,
			date: PropTypes.instanceOf(Date).isRequired
		})).isRequired,
		open: PropTypes.func.isRequired,
		close: PropTypes.func.isRequired,
		modify: PropTypes.func.isRequired,
		push: PropTypes.func.isRequired,
	}

	create = () => {
		let { close, modify, push } = this.props;
		close();
		modify(false);
		push("/stream");
	}

	open = (file) => {
		let { set, open, modify, push } = this.props;
		set(read(file));
		open(file);
		modify(false);
		push("/stream");
	}

	recover = () => this.props.push("/stream")

	render() {
		let { directory, recent } = this.props;
		return (
			<Home onCreate={this.create}
				  onOpen={this.open}
				  onRecover={this.recover}
				  directory={directory}
				  recent={recent} />
		)
	}
}

const mapper = (state) => ({
	recent: state.recent,
	directory: getDirectory(state)
})

export default connect(
	mapper,
	{ set, open, close, modify, push }
)(HomeContainer)
