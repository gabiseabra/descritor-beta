import React, { Component, PropTypes } from "react"
import { connect } from "react-redux"

export class App extends Component {
	static propTypes = {
		file: PropTypes.string,
		modified: PropTypes.number.isRequired
	}

	setTitle = () => {
		let { file, modified } = this.props;
		let title = "Descritor Beta";
		console.log(modified);
		if(file) title += " - " + file;
		if(modified > 0) title += " *";
		document.title = title;
	}

	componentWillReceiveProps() { this.setTitle() }

	componentDidMount() { this.setTitle() }

	render() {
		let { children } = this.props;
		return (
			<div className="App">
				{children}
			</div>
		)
	}
}

const mapper = (state) => ({
	file: state.document,
	modified: state.modified
})

export default connect(mapper)(App)
