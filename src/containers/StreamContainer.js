import React, { Component, PropTypes } from "react"
import { connect } from "react-redux"
import { push }from "react-router-redux"
import { open, close, set, remove, modify, remember } from "../actions"
import { getDirectory } from "../reducers"
import { write } from "../api/Files";
import Stream from "../components/Stream"
import fs from "fs"
import path from "path"

export class StreamContainer extends Component {
	static propTypes = {
		file: PropTypes.string,
		directory: PropTypes.string,
		modified: PropTypes.number.isRequired,
		children: PropTypes.array.isRequired,
		open: PropTypes.func.isRequired,
		close: PropTypes.func.isRequired,
		set: PropTypes.func.isRequired,
		remove: PropTypes.func.isRequired,
		modify: PropTypes.func.isRequired,
		remember: PropTypes.func.isRequired,
		push: PropTypes.func.isRequired
	}

	save = file => {
		let { open, modify, remember, children } = this.props;
		write(file, children);
		modify(false);
		open(file);
		remember(file);
	}

	remove = id => {
		let { remove, modify } = this.props;
		remove(id);
		modify(true);
	}

	edit = id => {
		let { push } = this.props;
		push(`/book/${id}`);
	}

	return = () => {
		let { close, set, modify } = this.props;
		set([]);
		close();
		modify(false);
	}

	render() {
		let { file, directory, modified, children } = this.props;
		return (
			<Stream file={file}
					directory={directory}
					modified={modified}
					onRemove={this.remove}
					onEdit={this.edit}
					onSave={this.save}
					onReturn={this.return}
					onPrint={window.print}>
				{children}
			</Stream>
		)
	}
}

export const mapper = (state) => ({
	file: state.document,
	modified: state.modified,
	children: state.stream,
	directory: getDirectory(state)
})

export default connect(
	mapper,
	{ set, open, close, remove, modify, remember, push }
)(StreamContainer)
