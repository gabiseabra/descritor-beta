import React, { Component, PropTypes } from "react"
import { connect } from "react-redux"
import { push }from "react-router-redux"
import { add, update, modify } from "../actions"
import { getBook } from "../reducers"
import Book from "../components/Book"

export class BookContainer extends Component {
	static propTypes = {
		params: PropTypes.shape({
			id: PropTypes.string
		}),
		book: PropTypes.object,
		add: PropTypes.func.isRequired,
		update: PropTypes.func.isRequired,
		modify: PropTypes.func.isRequired,
		push: PropTypes.func.isRequired
	}

	static defaultProps = {
		params: {}
	}

	submit = book => {
		let { add, update, modify, push } = this.props;
		if("id" in this.props.params) {
			update(book);
		} else {
			add(book);
		}
		modify(true);
		push("/stream");
	}

	render() {
		let { book } = this.props;
		return (
			<Book onSubmit={this.submit}
				  state={book} />
		)
	}
}

export const mapper = (state, props) => ({
	book: "id" in props.params ? getBook(state, Number(props.params.id)) : undefined
})

export default connect(
	mapper,
	{ add, update, modify, push }
)(BookContainer)
