import { formatter } from "../conf"

export default class Book {
	constructor(props) {
		Object.assign(this, props);
	}

	toString = () => formatter(this)
}

export const stringify = props => formatter(props || {});
