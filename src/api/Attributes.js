import { PropTypes } from "react"

const values = obj => {
	let values = [];
	for(let prop of Object.keys(obj)) values.push(obj[prop]);
	return values;
}

export const condition = {
	AS_ISSUED: "asIssued",
	AS_NEW:    "asNew",
	EXCELLENT: "excellent",
	GOOD:      "good",
	REGULAR:   "regular",
	POOR:      "poor"
}

export const binding = {
	PAPERBACK: "paperback",
	HARDCOVER: "hardcover",
	REBOUND:   "rebound"
}

export const cover = {
	LOOSE:         "loose",
	RIPPED:        "ripped",
	RUBBED:        "rubbed",
	DAMAGED:       "damaged",
	MISSING_FRONT: "missingFront",
	MISSING_BACK:  "missingBack"
}

export const textblock = {
	FOXED: 			 "foxed",
	SLIGHT_YELLOWED: "slightYellowed",
	YELLOWED: 		 "yellowed"
}

export const edge = {
	FOXED: 			 "foxed",
	SLIGHT_YELLOWED: "slightYellowed",
	YELLOWED: 		 "yellowed"
}

export const marks = {
	PEN:    "pen",
	PENCIL: "pencil",
	MAKER:  "marker"
}

export default {
	featured: PropTypes.string,
	title: PropTypes.string,
	author: PropTypes.string,
	editor: PropTypes.string,
	illustrator: PropTypes.string,
	publisher: PropTypes.string,
	edition: PropTypes.number,
	year: PropTypes.number,
	volumes: PropTypes.number,
	category: PropTypes.string,
	languages: PropTypes.arrayOf(PropTypes.string),
	pageCount: PropTypes.number,
	width: PropTypes.number,
	height: PropTypes.number,
	copyCount: PropTypes.number,
	copyNumber: PropTypes.number,
	signedBy: PropTypes.arrayOf(PropTypes.string),
	description: PropTypes.string,
	condition: PropTypes.oneOf(values(condition)),
	binding: PropTypes.oneOf(values(binding)),
	cover: PropTypes.arrayOf(PropTypes.oneOf(values(cover))),
	textblock: PropTypes.oneOf(values(textblock)),
	edge: PropTypes.oneOf(values(edge)),
	marks: PropTypes.arrayOf(PropTypes.oneOf(values(marks)))
}
