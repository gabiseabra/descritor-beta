import json from "./json"
import path from "path"
import { remote } from "electron"

const fileTypes = [ json ]

const defaultType = "json";

const filters = fileTypes.map(type => ({
	name: `${type.label} (*.${type.extensions.join(", *.")})`,
	extensions: type.extensions
}))

const getTypeFromExtension = ext => {
	ext = ext.toLowerCase().replace(/^\./g, "");
	for(let i = 0; i < fileTypes.length; ++i) {
		if(fileTypes[i].extensions.includes(ext)) {
			return fileTypes[i];
		}
	}
}

export const open = path => {
	let files = remote.dialog.showOpenDialog({
		filters,
		defaultPath: path,
		properties: ["openFile"]
	});
	if(files && files.length) {
		return files[0];
	}
}

export const save = path => remote.dialog.showSaveDialog({
		filters,
		defaultPath: path
	})

export const read = file => {
	let type = getTypeFromExtension(path.extname(file));
	if(!type) {
		throw new Error("Unsupported file type.");
	}
	return type.read(file);
}


export const write = (file, stream) => {
	let ext = path.extname(file);
	if(!ext) {
		ext   = defaultType;
		file += "." + ext;
	}
	let type = getTypeFromExtension(ext);
	if(!type) {
		throw new Error("Unsupported file type.");
	}
	type.write(file, stream);
}
