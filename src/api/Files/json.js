import fs from "fs"

const extensions = [ "json" ]

const label = "JSON"

const read = file => JSON.parse(fs.readFileSync(file))

const write = (file, stream) => fs.writeFileSync(file, JSON.stringify(stream))

export default { extensions, label, read, write }
