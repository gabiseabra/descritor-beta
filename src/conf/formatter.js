import * as attr from "../api/Attributes"

const ucFirst = value =>
	value.charAt(0).toUpperCase() +
	value.substring(1)

const list = value => {
	if(value.length > 1) {
		return value.slice(0, -1).join(", ") + " e " + value[value.length-1];
	} else if(value.length) {
		return value[0]
	}
}

const compose = (options, props) => {
	let out = [];
	options.forEach(list => {
		let phrases = [];
		list.forEach(item => {
			if(typeof item === "function") {
				let text = item(props);
				if(typeof text !== "undefined") {
					phrases.push(text);
				}
			} else if(item in props) {
				phrases.push(props[item]);
			}
		});
		if(phrases.length) {
			out.push(ucFirst(phrases.join(", ")));
		}
	});
	if(out.length) {
		return out.join(". ") + ".";
	}
}

class Stringifyer {
	constructor(props) {
		this.props = props;
		return new Proxy(this, {
			get(target, prop) {
				if(prop in target) {
					return target[prop];
				} else if(target.has(prop)) {
					return target.val(prop);
				}
			},
			has(target, prop) {
				return target.has(prop);
			}
		});
	}

	has(prop) {
		switch(prop) {
			case "featured":
				let val = this.props["featured"];
				return (val && val !== "featured" && this.has(val));
			case "size":
				return ("width"	 in this.props && "height" in this.props);
			case "languages":
			case "signedBy":
				return (prop in this.props && typeof this.props[prop] === "object" && this.props[prop].length);
			default:
				return (prop in this.props && this.props[prop]);
		}
	}

	val(prop) {
		return this.props[prop];
	}

	get title() { return ucFirst(this.props.title || "") }
	get author() { return `de ${this.props.author}` }
	get editor() { return `editado por ${this.props.editor}` }
	get illustrator() { return `ilustrado por ${this.props.illustrator}` }
	get publisher() { return `editora ${this.props.publisher}` }
	get edition() { return `${this.props.edition}ª edição` }
	get pageCount() { return `${this.props.pageCount} páginas` }
	get width() { return `${this.props.width}cm` }
	get height() { return `${this.props.height}cm` }
	get size() { return `${this.width} x ${this.height}` }
	get description() { return (this.props.description || "").trim() }
	get volumes() { return `${this.props.volumes} volumes` }
	get languages() { return `em ${list(this.props.languages)}` }
	get copyNumber() {
		let out = `exemplar numerado #${this.props.copyNumber}`;
		if(this.has("copyCount")) {
			out += ` de ${this.props.copyCount}`;
		}
		return out;
	}
	get signedBy() {
		let { signedBy } = this.props;
		let out = "assinado ";
		let named = [], unnamed = [];
		for(let i = 0; i < signedBy.length; ++i) {
			switch(signedBy[i]) {
				case "author": unnamed.push("autor"); break;
				case "editor": unnamed.push("editor"); break;
				case "illustrator": unnamed.push("ilustrador"); break;
				case "owner": unnamed.push("antigo dono"); break;
				default: named.push(signedBy[i]); break;
			}
		}
		out += (unnamed.length ? "pelo " : "por ");
		out += list(unnamed.concat(named));
		return out;
	}
	get condition() {
		let text;
		switch(this.props.condition) {
			case attr.condition.AS_ISSUED: text = "novo"; break;
			case attr.condition.AS_NEW: text = "como novo"; break;
			case attr.condition.EXCELLENT: text = "em excelente estado de conservação"; break;
			case attr.condition.GOOD: text = "em bom estado de conservação"; break;
			case attr.condition.REGULAR: text = "em estado de conservação regular"; break;
			case attr.condition.POOR: text = "em mal estado de conservação"; break;
		}
		return "livro " + text;
	}
	get condition() {
		let text;
		switch(this.props.condition) {
			case attr.condition.AS_ISSUED: text = "novo"; break;
			case attr.condition.AS_NEW: text = "como novo"; break;
			case attr.condition.EXCELLENT: text = "em excelente estado de conservação"; break;
			case attr.condition.GOOD: text = "em bom estado de conservação"; break;
			case attr.condition.REGULAR: text = "em estado de conservação regular"; break;
			case attr.condition.POOR: text = "em mal estado de conservação"; break;
		}
		return "livro " + text;
	}
	get binding() {
		switch(this.props.binding) {
			case attr.binding.PAPERBACK: return "brochura";
			case attr.binding.HARDCOVER: return "capa dura";
			case attr.binding.REBOUND: return "reencadernado";
		}		
	}
	get cover() {
		let out = [], prefix = "";
		let props = (this.props.cover || []);
		if(props.indexOf(attr.cover.LOOSE) !== -1) {
			prefix = "capa ";
			out.push("solta");
		}
		if(props.indexOf(attr.cover.RIPPED) !== -1) {
			prefix = "capa ";
			out.push("rasgada");
		}
		if(props.indexOf(attr.cover.DAMAGED) !== -1) {
			out.push("desgaste na capa e contracapa");
		}
		if(props.indexOf(attr.cover.RUBBED) !== -1) {
			out.push("desgaste nas bordas");
		}
		if(props.indexOf(attr.cover.MISSING_FRONT) !== -1 &&
		   props.indexOf(attr.cover.MISSING_BACK) !== -1) {
			out.push("faltando capa frontal e traseira")
		} else if(props.indexOf(attr.cover.MISSING_FRONT) !== -1) {
			out.push("faltando capa frontal")
		} else if(props.indexOf(attr.cover.MISSING_BACK) !== -1) {
			out.push("faltando capa traseira")
		}
		return prefix + out.join(", ");
	}
	get textblock() {
		switch(this.props.textblock) {
			case attr.textblock.FOXED: return "miolo com pigmentação";
			case attr.textblock.SLIGHT_YELLOWED: return "miolo levemente amarelado";
			case attr.textblock.YELLOWED: return "miolo amarelado";
		}
	}
	get edge() {
		switch(this.props.edge) {
			case attr.edge.FOXED: return "corte com pigmentação";
			case attr.edge.SLIGHT_YELLOWED: return "corte levemente amarelado";
			case attr.edge.YELLOWED: return "corte amarelado";
		}
	}
	get marks() {
		let out = [];
		let props = (this.props.marks || []);
		if(props.indexOf(attr.marks.PEN) !== -1) out.push("caneta");
		if(props.indexOf(attr.marks.PENCIL) !== -1) out.push("lápis");
		if(props.indexOf(attr.marks.MARKER) !== -1) out.push("marca-texto");
		return "marcações à " + list(out);
	}
}

const Paragraphs = [
	// Featured
	props => {
		let out = [];
		if("featured" in props) out.push(props[props.featured]);
		if("title" in props) out.push(props.title);
		return out.join(", ");
	},
	// Book data
	props => compose([
		[ ("author" in props ? "obra " + props.author : undefined),
		  "publisher", "edition", "year", "languages" ],
		[ "copyNumber", "signedBy" ],
		[ "editor", "illustrator" ],
		[ "category", "pageCount", "size" ]
	], props),
	// Specimen data
	props => compose([
		[ "condition", "binding", "cover" ],
		[ "marks", "textblock", "edge" ]
	], props),
	// Description
	props => props.description
]

export default function(props) {
	let i   = 0,
		out = [];
	props = new Stringifyer(props || {});
	for(; i < Paragraphs.length; ++i) {
		let text = Paragraphs[i](props);
		if(text) {
			text = text.trim();
			out.push(ucFirst(text));
		}
	}

	return out.join("  ");
}
