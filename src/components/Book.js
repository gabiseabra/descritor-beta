import React, { Component, PropTypes } from "react"
import { Link } from "react-router"
import { Creatable } from "react-select"
import { categories, languages } from "../conf"
import * as attr from "../api/Attributes"
import { stringify } from "../api/Book"

const Context = {
	state: PropTypes.object.isRequired,
	labels: PropTypes.object.isRequired,
	change: PropTypes.func.isRequired
}

class Result extends Component {
	static propTypes = {
		collapsed: PropTypes.bool,
		className: PropTypes.string,
		children: PropTypes.node.isRequired
	}

	state = {
		collapsed: false
	}

	changeState = () => this.setState({ collapsed: !this.state.collapsed });

	componentWillReceiveProps(next) {
		if(next.hasOwnProperty("collapsed")) {
			this.state.collapsed = Boolean(next.collapsed);
		}
	}

	render() {
		let { children, className } = this.props;
		className  = (className || "");
		if(this.state.collapsed) className += " collapsed";
		if(!children) className += " empty";
		return (
			<div className={className + " Result collapsable"}>
				<div className="handle" onClick={this.changeState}>
					<div className="icon fa fa-angle-down" />
				</div>
				<div className="description area">
					<pre>{children}</pre>
				</div>
			</div>
		)
	}
}

class Field extends Component {
	static contextTypes = Context

	static propTypes = {
		name: PropTypes.string,
		required: PropTypes.bool,
		className: PropTypes.string,
		children: PropTypes.element.isRequired
	}

	change = e => {
		let { name, value } = e.target;
		this.context.change(name, value || undefined);
	}

	render() {
		let { name, required, children, className } = this.props;
		let { state: { featured }, change } = this.context;
		if(typeof name === "undefined") name = children.props.name;
		return (
			<div className={(className || "") + " Field"}>
				{name &&
				 <label className="feature">
					<input type="radio"
							name="featured"
							value={name}
							onChange={this.change} />
					<div className="icon fa fa-star" />
				 </label>
				}
				{children}
			</div>
		)
	}
}

class Options extends Component {
	static contextTypes = Context

	static propTypes = {
		name: PropTypes.string.isRequired,
		multi: PropTypes.bool.isRequired,
		options: PropTypes.arrayOf(PropTypes.shape({
			label: PropTypes.string.isRequired,
			value: PropTypes.string.isRequired
		})).isRequired,
		className: PropTypes.string
	}

	static defaultProps = {
		multi: false
	}

	change = e => {
		let { name, multi } = this.props;
		let { checked, value } = e.target;
		let newVal;
		if(!multi) {
			newVal = checked ? value : undefined;
		} else {
			newVal = this.context.state[name] || [];
			if(checked) newVal.push(value);
			else newVal = newVal.filter(val => val !== value);
		}
		this.context.change(name, newVal);
	}

	isChecked = value => {
		let { name, multi } = this.props;
		let { state } = this.context;
		if(multi) {
			return (state[name] || []).indexOf(value) !== -1;
		} else {
			return state[name] === value;
		}
	}

	render() {
		let { name, className, options, multi, ...props }  = this.props;
		let { labels } = this.context;
		if(props.disabled) className = (className || "") + " disabled";
		return (
			<div className={(className || "") + " Options"}>
				<div className="label">{labels[name]}</div>
				<div className="options">
					{options.map(option => 
					<label className="option" key={option.value}>
						<input type={multi ? "checkbox" : "radio"}
							name={name}
							value={option.value}
							checked={this.isChecked(option.value)}
							onChange={this.change}
							{...props} />
						<span className="label">{option.label}</span>
					</label>
					)}
				</div>
			</div>
		)
	}
}

class Textarea extends Component {
	static contextTypes = Context

	static propTypes = {
		name: PropTypes.string.isRequired,
		className: PropTypes.string
	}

	change = e => {
		let { name, value } = e.target;
		this.context.change(name, value);
	}

	render() {
		let { type, name, className, ...props }  = this.props;
		let { labels, state, change } = this.context;
		let required = this.props.required || false;
		return(
			<label className={(className || "") + " Textarea"}>
				<textarea className={(state[name] ? "has-value" : "")}
						  name={name}
						  value={state[name] || ""}
						  onChange={this.change}
						  {...props} />
				<div className="label">{labels[name]}</div>
			</label>
		)
	}
}

class Select extends Component {
	static contextTypes = Context

	static propTypes = {
		name: PropTypes.string.isRequired,
		multi: PropTypes.bool.isRequired
	}

	static defaultProps = {
		multi: false,
		clearable: true,
		placeholder: ""
	}

	textCreator = label => `"${label}"`

	change = value => {
		let { name, multi } = this.props;
		if(!value) {
			value = undefined;
		} else if(multi) {
			value = value.map(value => value.value);
		} else {
			value = value.value;
		}
		this.context.change(name, value);
	}

	render() {
		let { name, className, ...props }  = this.props;
		let { labels, state } = this.context;
		return (
			<label className={(className || "") + " SelectContainer"}>
				<Creatable name={name}
						   promptTextCreator={this.textCreator}
						   value={state[name]}
						   options={state.options[name]}
						   onChange={this.change}
						   {...props}/>
				<div className="label">{labels[name]}</div>
			</label>
		)
	}
}

class Input extends Component {
	static contextTypes = Context

	static propTypes = {
		type: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
		className: PropTypes.string
	}

	static defaultProps = {
		type: "text"
	}

	change = e => {
		let { name, value, type } = e.target;
		if(value === "") {
			value = undefined;
		} else if(type === "number") {
			value = Number(value);
		}
		this.context.change(name, value);
	}

	render() {
		let { type, name, className, ...props }  = this.props;
		let { labels, state } = this.context;
		let required = this.props.required || false;
		return(
			<label className={(className || "") + (required ? " required" : "") + " Input"}>
				<input type={type}
					   name={name}
					   value={state[name] || ""}
					   onChange={this.change}
					   {...props} />
				<div className="label">{labels[name]}</div>
			</label>
		)
	}
}

export default class Book extends Component {
	static childContextTypes = Context

	static propTypes = {
		onSubmit: PropTypes.func.isRequired,
		shuffle: PropTypes.bool.isRequired,
		labels: PropTypes.object.isRequired,
		categories: PropTypes.arrayOf(PropTypes.string).isRequired,
		languages: PropTypes.arrayOf(PropTypes.string).isRequired,
		state: PropTypes.object
	}

	static defaultProps = {
		labels: {
			featured: "Destaque",
			title: "Título",
			author: "Autor",
			editor: "Editor",
			illustrator: "Ilustrador",
			publisher: "Editora",
			edition: "# Edição",
			year: "Ano de Publicação",
			volumes: "Número de Volumes",
			category: "Categoria",
			languages: "Língua",
			pageCount: "Número de Páginas",
			width: "Largura",
			height: "Altura",
			copyCount: "Número de Exemplares",
			copyNumber: "# Exemplar",
			signedBy: "Assinado Por",
			//description: "Comentários",
			condition: "Condição",
			binding: "Encadernação",
			cover: "Capa",
			textblock: "Miolo",
			edge: "Corte",
			marks: "Marcações",
			spine: "Lombada"
		},
		shuffle: process.env.NODE_ENV === "development",
		languages,
		categories
	}

	getChildContext = () => ({
		state: this.state,
		labels: this.props.labels,
		change: this.change
	})

	change = (prop, value) => this.setState({ [prop]: value || undefined })

	shuffle = () => {
		let faker = require("faker");
		return this.setState({
			title: faker.lorem.sentence(),
			author: faker.name.findName(),
			editor: faker.random.boolean() ? faker.name.findName() : undefined,
			illustrator: faker.random.boolean() ? faker.name.findName() : undefined,
			publisher: faker.company.companyName(),
			edition: faker.random.number(10),
			year: faker.random.number({ min: 1900, max: 2017 }),
			category: faker.random.arrayElement(categories),
			pageCount: faker.random.number({ min: 200, max: 1000 }),
			width: faker.random.number({ min: 100, max: 150 }),
			height: faker.random.number({ min: 100, max: 150 }),
			description: faker.lorem.paragraphs(1)
		});
	}

	submit = e => {
		let book = Object.assign({}, this.state);
		delete book.options;
		e.preventDefault();
		this.props.onSubmit(book);
	}

	constructor(props) {
		let mapper = value => ({ label: value, value });
		super(props);
		this.state = {
			options: {
				category: props.categories.map(mapper),
				languages: props.languages.map(mapper),
				signedBy: [
					{ value: "author", label: props.labels.author },
					{ value: "editor", label: props.labels.editor },
					{ value: "illustrator", label: props.labels.illustrator },
					{ value: "owner", label: "Antigo dono" }
				]
			}
		};
	}

	componentWillReceiveProps(next) {
		if(next.state) {
			this.setState(next.state);
		}
	}

	render() {
		return (
			<form onSubmit={e => e.preventDefault()} className="page Book">
				<header>
					<Link to="/stream"
						  title="Voltar"
						  className="previous fa fa-chevron-left" />
					Adicionar Livro
				</header>
				<section className="content">
					{this.renderBookFieldset()}
					{this.renderSpecimenFieldset()}
					{this.renderCommentsFieldset()}
				</section>
				<Result>
					{stringify(this.state) || "Descrição..."}
				</Result>
				<footer>
					{this.props.shuffle &&
					 <div className="icon shuffle fa fa-random" onClick={this.shuffle}/>
					}
					<input type="submit" value="Adicionar Livro" onClick={this.submit} />
				</footer>
			</form>
		)
	}

	renderBookFieldset = () => (
		<fieldset>
			<label className="label">Livro</label>
			<Field name=""><Input type="text" name="title"/></Field>
			<Field><Input type="text" name="publisher"/></Field>
			<Field name="edition">
				<div className="row">
					<Input type="number" name="year" min={0}/>
					<Input type="number" name="edition" min={1}/>
				</div>
			</Field>
			<Field><Input type="number" name="volumes"/></Field>
			<Field><Input type="text" name="author"/></Field>
			<Field><Input type="text" name="editor"/></Field>
			<Field><Input type="text" name="illustrator"/></Field>
			<Field><Select name="category" /></Field>
			<Field><Select name="languages" multi={true} /></Field>
			<Field><Select name="signedBy" multi={true} /></Field>
			<Field><Input type="number" name="pageCount" min={0}/></Field>
			<Field name="size">
				<div className="row">
					<Input type="number" name="width" min={0}/>
					<Input type="number" name="height" min={0}/>
				</div>
			</Field>
			<Field name="copyNumber">
				<div className="row">
				<Input type="number" name="copyCount" min={0}/>
					<Input type="number" name="copyNumber" min={0}/>
				</div>
			</Field>
		</fieldset>
	)

	renderSpecimenFieldset = () => {
		let isDisabled = !this.state.condition || this.state.condition === attr.condition.AS_ISSUED;
		return (
			<fieldset>
				<label className="label">Exemplar</label>
				<Field name="">
					<Options name="condition"
							options={[
								{ "value": attr.condition.AS_ISSUED, "label": "Novo" },
								{ "value": attr.condition.AS_NEW,    "label": "Como novo" },
								{ "value": attr.condition.EXCELLENT, "label": "Excelente" },
								{ "value": attr.condition.GOOD,      "label": "Boa" },
								{ "value": attr.condition.REGULAR,   "label": "Regular" },
								{ "value": attr.condition.POOR,      "label": "Ruim" }
							]} />
				</Field>
				<Field name="">
					<Options name="binding"
							options={[
								{ "value": attr.binding.PAPERBACK, "label": "Brochura" },
								{ "value": attr.binding.HARDCOVER, "label": "Capa dura" },
								{ "value": attr.binding.REBOUND,   "label": "Reencadernado" }
							]} />
				</Field>
				<Field name="">
					<Options name="cover"
							multi={true}
							disabled={isDisabled}
							options={[
								{ "value": attr.cover.LOOSE,         "label": "Capa solta" },
								{ "value": attr.cover.RIPPED,        "label": "Capa rasgada" },
								{ "value": attr.cover.DAMAGED,       "label": "Desgastes na capa e contracapa" },
								{ "value": attr.cover.RUBBED,        "label": "Desgastes nas bordas" },
								{ "value": attr.cover.MISSING_FRONT, "label": "Faltando capa frontal" },
								{ "value": attr.cover.MISSING_BACK,  "label": "Faltando capa traseira" },
							]} />
				</Field>
				<Field name="">
					<Options name="textblock"
							disabled={isDisabled}
							options={[
								{ "value": attr.textblock.FOXED,           "label": "Com pigmentação" },
								{ "value": attr.textblock.SLIGHT_YELLOWED, "label": "Levemente amarelado" },
								{ "value": attr.textblock.YELLOWED,        "label": "Amarelado" }
							]} />
				</Field>
				<Field name="">
					<Options name="edge"
							disabled={isDisabled}
							options={[
								{ "value": 
								attr.edge.FOXED,           "label": "Com pigmentação" },
								{ "value": attr.edge.SLIGHT_YELLOWED, "label": "Levemente amarelado" },
								{ "value": attr.edge.YELLOWED,        "label": "Amarelado" }
							]} />
				</Field>
				<Field name="">
					<Options name="marks"
							multi={true}
							disabled={isDisabled}
							options={[
								{ "value": attr.marks.PEN,    "label": "Marcações à caneta" },
								{ "value": attr.marks.PENCIL, "label": "Marcações à lápis" },
								{ "value": attr.marks.MAKER,  "label": "Marcações à marca-texto" }
							]} />
				</Field>
			</fieldset>
		);
	}

	renderCommentsFieldset = () => (
		<fieldset>
			<label className="label">Comentários</label>
			<Field name=""><Textarea name="description" /></Field>
		</fieldset>
	)
}