import React, { Component, PropTypes } from "react"
import { open } from "../api/Files"
import dateFormat from "dateformat"

export default class Home extends Component {
	static propTypes = {
		directory: PropTypes.string,
		format: PropTypes.string.isRequired,
		recover: PropTypes.instanceOf(Date),
		recent: PropTypes.arrayOf(PropTypes.shape({
				file: PropTypes.string.isRequired,
				date: PropTypes.instanceOf(Date).isRequired
			})).isRequired,
		onCreate: PropTypes.func.isRequired,
		onOpen: PropTypes.func.isRequired,
		onRecover: PropTypes.func.isRequired
	}

	static defaultProps = {
		format: "dd/mm/yyyy H:MM",
		recent: []
	}

	open = () => {
		let { directory, onOpen } = this.props;
		let file = open(directory);
		if(file) {
			onOpen(file);
		}
	}

	render() {
		let { onCreate, onOpen, onRecover, recover, recent, format } = this.props;
		return (
			<div className="page Home">
				{recover &&
				 <div className="recover" onClick={onRecover}>
					Recuperar documento não salvo <span className="date">{dateFormat(recover, format)}</span>
				 </div>}
				<nav className="content">
					<button onClick={onCreate}>Novo Arquivo</button>
					<button onClick={this.open}>Abrir Arquivo...</button>
				</nav>
				{recent.length > 0 &&
				  <footer className="recent">
					  <div className="title">Documentos Recentes</div>
					  <ul className="files">
						  {recent.map(({ file, date }) =>
							  <li key={file} onClick={() => onOpen(file)}>
								  <span className="file" title={file}>{file}</span>
								  <span className="date">{dateFormat(date, format)}</span>
							  </li>
						  )}
					  </ul>
				  </footer>
				}
			</div>
		)
	}
}