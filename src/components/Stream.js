import React, { Component, PropTypes } from "react"
import { Link } from "react-router"
import { save } from "../api/Files"
import { remote } from "electron"
import keydown from "react-keydown"

const Context = {
	onRemove: PropTypes.func.isRequired,
	onEdit: PropTypes.func.isRequired
}

function escapeRegExp(str) {
	return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

class Filters extends Component {
	static propTypes = {
		className: PropTypes.string,
		count: PropTypes.number,
		onChange: PropTypes.func.isRequired
	}

	state = {
		collapsed: false,
		disabled: false,
		filters: {}
	}

	changeState = () => this.setState({ collapsed: !this.state.collapsed })

	toggleFilters = () => this.setState({ disabled: !this.state.disabled }, this.onChange)

	changeFilter = (e) => {
		let { name, value, type } = e.target;
		const { filters } = this.state
		if(value === "") {
			value = undefined;
		} else if(type === "number") {
			value = Number(value);
		}
		this.setState({
			filters: {
				...filters,
				[name]: value
			}
		}, this.onChange)
	}

	onChange = () => this.props.onChange(
		!this.state.disabled ?
		this.state.filters :
		{}
	)

	render() {
		let { count, className } = this.props;
		const { filters } = this.state
		className  = (className || "");
		if(this.state.collapsed) className += " collapsed";
		if(this.state.disabled) className += " disabled";
		return (
			<div className={className + " Filters collapsable"}>
				<div className="area">
					<form onSubmit={e => e.preventDefault()}>
						<section>
							<label className="Input">
								<input
									type="text"
									name="title"
									onChange={this.changeFilter}
									value={filters.title || ""} />
								<div className="label">Título</div>
							</label>
							<label className="Input">
								<input
									type="text"
									name="author"
									onChange={this.changeFilter}
									value={filters.author || ""} />
								<div className="label">Autor</div>
							</label>
							<label className="Input">
								<input
									type="text"
									name="category"
									onChange={this.changeFilter}
									value={filters.category || ""} />
								<div className="label">Categoria</div>
							</label>
							<label className="Input">
								<input
									type="number"
									name="year"
									onChange={this.changeFilter}
									value={filters.year || ""} />
								<div className="label">Ano de Publicação</div>
							</label>
						</section>
						<button
							onClick={this.toggleFilters}
							title={this.state.disabled ? "Aplicar Filtros" : "Desativar Filtros"}>
							<div className="icon fa fa-search" />
						</button>
					</form>
					<div className="count">{count + " Resultados"}</div>
				</div>
				<div className="handle" onClick={this.changeState}>
					<div className="icon fa fa-angle-up" />
				</div>
			</div>
		)
	}
}

export class Node extends Component {
	static contextTypes = Context

	static propTypes = {
		book: PropTypes.object.isRequired,
		pos: PropTypes.number.isRequired
	}

	remove = () => this.context.onRemove(this.props.book.id)

	edit = () => this.context.onEdit(this.props.book.id)

	render() {
		let { book, pos } = this.props;
		return (
			<li className="Node">
				<header className="data">
					<div className="id">{pos}</div>
					<div className="year-title">
						<span className="title">{book.title}</span>
						{book.year && <span className="year">({book.year})</span>}
					</div>
					{book.author && <div className="author">{book.author}</div>}
					<div className="controls">
						<div onClick={this.edit} className="edit fa fa-pencil" />
						<div onClick={this.remove} className="remove fa fa-times" />
					</div>
				</header>
				<section className="description">
					<pre>{book.toString()}</pre>
				</section>
			</li>
		)
	}
}

export default class Stream extends Component {
	static childContextTypes = Context

	static propTypes = {
		children: PropTypes.array,
		file: PropTypes.string,
		directory: PropTypes.string,
		modified: PropTypes.number.isRequired,
		onRemove: PropTypes.func.isRequired,
		onEdit: PropTypes.func.isRequired,
		onSave: PropTypes.func.isRequired,
		onPrint: PropTypes.func.isRequired,
		onReturn: PropTypes.func.isRequired
	}

	state = {
		filters: {}
	}

	getChildContext = () => ({
		onRemove: this.props.onRemove,
		onEdit: this.props.onEdit
	})

	constructor(props) {
		super(props);
		this.save   = this.save.bind(this);
		this.saveAs = this.saveAs.bind(this);
		this.return = this.return.bind(this);
	}

	changeFilter = (filters) => this.setState({ filters }) && this.forceUpdate()

	getVisibleChildren = () => {
		const { children } = this.props;
		const filters = {}
		for(const prop in this.state.filters) {
			let value = this.state.filters[prop]
			if(typeof value === "string") {
				value = new RegExp(`${escapeRegExp(value)}`, "i")
			} else if(prop === "year") {
				value = new RegExp(`^${value}`)
			}
			filters[prop] = value;
		}
		const { author, title, category, year } = filters
		return children.filter((book) => {
			if(author && !author.test(book.author)) return false;
			if(title && !title.test(book.title)) return false;
			if(category && !category.test(book.category)) return false;
			if(year && !year.test(book.year.toString())) return false;
			return true;
		})
	}

	shouldClose = e => {
		let { file, modified, onSave } = this.props;
		if(modified) {
			let choice = remote.dialog.showMessageBox({
				type: "question",
				message: "Há modificações não salvas neste arquivo, deseja salvá-las antes de fechar?",
				cancelId: 0,
				defaultId: 1,
				buttons: [ "Cancelar", "Salvar", "Fechar" ]
			});
			if(choice === 0) {
				if(e) {
					e.preventDefault();
					e.stopPropagation();
					e.returnValue = "false";
				}
				return false;
			}
			if(choice === 1) {
				if(file) onSave(file);
				else this.saveAs();
			}
		}
		return true;
	}

	@keydown("ctrl+S")
	save() {
		let { onSave, file } = this.props;
		if(file) {
			onSave(file);
		} else {
			this.saveAs();
		}
	}

	@keydown("ctrl+shift+S")
	saveAs() {
		let { onSave, directory } = this.props;
		let file = save(directory);
		if(file) {
			onSave(file);
		}
	}

	return(e) {
		let { onReturn } = this.props;
		if(this.shouldClose(e)) onReturn();
	}

	componentDidMount() {
		window.addEventListener("beforeunload", this.shouldClose, false);
	}

	componentWillUnmount() {
		window.removeEventListener("beforeunload", this.shouldClose, false);
	}

	render() {
		const { file, onRemove, onSave, onPrint } = this.props;
		const children = this.getVisibleChildren();
		return (
			<div className="page Stream">
				<header>
					<Link to="/"
						  title="Voltar"
						  className="previous fa fa-chevron-left"
						  onClick={this.return}/>
					Livros
					<div className="print fa fa-print"
					 	 title="Imprimir"
					 	 onClick={onPrint}/>
				</header>
				<Filters onChange={this.changeFilter} count={children.length} />
				<section className="content">
					<ul className="items">
						{children.map((book, i) => <Node key={book.id} book={book} pos={i + 1} />)}
					</ul>
				</section>
				<footer>
					<Link to="/book"
						  title="Adicionar Livro"
						  className="add-book icon fa fa-plus" />
					<div className="file-options">
						<button className="button save"
						        disabled={!file}
						        onClick={() => file && onSave(file)}>Salvar</button>
						<button className="button save-as"
						        onClick={this.saveAs}>Salvar Como...</button>
					</div>
				</footer>
			</div>
		)
	}
}