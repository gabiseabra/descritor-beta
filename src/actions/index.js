export const ADD_BOOK = "ADD_BOOK"
export const DEL_BOOK = "DEL_BOOK"
export const EDIT_BOOK = "EDIT_BOOK"
export const SET_STREAM = "SET_STREAM"
export const SET_MODIFIED = "SET_MODIFIED"
export const SET_DOCUMENT = "SET_DOCUMENT"
export const PUSH_DOCUMENT = "PUSH_DOCUMENT"
export const SET_RECENT = "SET_RECENT"
export const add = (book) => ({ type: ADD_BOOK, book })
export const remove = (id) => ({ type: DEL_BOOK, id })
export const update = (book, id = undefined) => ({ type: EDIT_BOOK, book: { ...book, id: Number(id || book.id) } })
export const set = (stream) => ({ type: SET_STREAM, stream })
export const clear = () => ({ type: SET_STREAM, stream: [] })
export const modify = (modified) => ({ type: SET_MODIFIED, modified })
export const open = (file) => ({ type: SET_DOCUMENT, file })
export const close = () => ({ type: SET_DOCUMENT, file: null })
export const remember = (file) => ({ type: PUSH_DOCUMENT, file })
