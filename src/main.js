import { app, BrowserWindow } from "electron"
import contextMenu from "electron-context-menu"

let win;

function init() {
	win = new BrowserWindow({
		width: 500,
		minWidth: 400,
		height: 600,
		minHeight: 400,
		center: true,
		icon: "./icons/icon.png"
	});

	win.on("closed", () => { win = null });

	win.loadURL(`file://${__dirname}/index.html`);

	contextMenu({
		window: win,
		labels: {
			cut: "Cortar",
			copy: "Copiar",
			paste: "Colar",
			save: "Salvar",
			copyLink: "Copiar Link",
			inspect: "Inspecionar Elemento"
		},
		showInspectElement: process.env.NODE_ENV === "development"
	});

	if(process.env.NODE_ENV === "development") {
		win.webContents.openDevTools();
	}
}

app.on("ready", init);

app.on("activate", () => { if(win === null) init() });

app.on("window-all-closed", () => { if(process.platform !== "darwin") app.quit() });
