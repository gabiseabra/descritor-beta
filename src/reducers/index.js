import { combineReducers } from "redux"
import { routerReducer as routing } from "react-router-redux";
import * as actions from "../actions"
import { REHYDRATE } from "redux-persist/constants"
import Book from "../api/Book"
import path from "path"
import fs from "fs"

const stream = (state = [], action) => {
	switch(action.type) {
		case actions.ADD_BOOK:
			return [
				...state,
				new Book({
					id: state.reduce((max, book) => Math.max(book.id, max), -1) + 1,
					...action.book
				})
			];
		case actions.DEL_BOOK:
			return state.filter(book => (book.id !== action.id));
		case actions.EDIT_BOOK:
			return state.map(book => (book.id === action.book.id ? new Book(action.book) : book));
		case REHYDRATE: action.stream = action.payload.stream;
		case actions.SET_STREAM:
			let i = action.stream.reduce((max, book) => Math.max(book.id || -1, max), -1) + 1;
			return action.stream.map(book => {
				if(!book.hasOwnProperty("id")) book.id = i++;
				return new Book(book);
			});
		default:
			return state;
	}
}

const modified = (state = 0, action) => {
	switch(action.type) {
		case actions.SET_MODIFIED:
			return (action.modified === true ? Date.now() : 0);
		default:
			return state;
	}
}

const document = (state = null, action) => {
	switch(action.type) {
		case actions.SET_DOCUMENT:
			return action.file;
		default:
			return state;
	}
}

const recent = (state = [], action) => {
	switch(action.type) {
		case REHYDRATE:
			return action.payload.recent
				.filter(({ file }) => fs.existsSync(file))
				.map(({ file, date }) => ({ file, date: new Date(date) }));
		case actions.PUSH_DOCUMENT:
			return [
				{ file: action.file, date: new Date() },
				...state.filter(({ file }) => file !== action.file )
			].slice(0, 5);
		default:
			return state;
	}
}

export default combineReducers({ stream, modified, document, recent, routing })

export const getBook = (state, id) => state.stream.find(book => book.id === Number(id))

export const getDirectory = state => {
	if(state.document) {
		return path.dirname(state.document);
	} else if(state.recent.length) {
		return path.dirname(state.recent[0].file)
	}
}
